resource "yandex_mdb_postgresql_cluster" "postgresql" {
  name        = var.yandex-db-name
  folder_id   = var.yandex-folder-id
  environment = var.db-environment
  network_id  = yandex_vpc_network.network_terraform.id

  config {
    version = var.db-version
    resources {
      resource_preset_id = var.db-platform-preset
      disk_type_id       = var.db-disk-type
      disk_size          = var.db-disk-size
    }
  }

  maintenance_window {
    type = var.db-maintenance.type
    day  = var.db-maintenance.day
    hour = var.db-maintenance.hour
  }

  host {
    zone      = var.zone
    subnet_id = yandex_vpc_subnet.subnet_terraform.id
  }
}

resource "yandex_mdb_postgresql_database" "db-opencart" {
  cluster_id = yandex_mdb_postgresql_cluster.postgresql.id
  name       = var.opencart-db.db_name
  owner      = var.opencart-db.db_user_name
}

resource "yandex_mdb_postgresql_user" "user-opencart" {
  cluster_id = yandex_mdb_postgresql_cluster.postgresql.id
  name       = var.opencart-db.db_user_name
  password   = var.opencart-db.db_user_password
  conn_limit = var.db-conn-limit
}

locals {
  dbuser = tolist(yandex_mdb_postgresql_user.user-opencart.*.name)[0]
  dbpassword = tolist(yandex_mdb_postgresql_user.user-opencart.*.password)[0]
  dbhosts = yandex_mdb_postgresql_cluster.postgresql.host.*.fqdn
  dbname = tolist(yandex_mdb_postgresql_database.db-opencart.*.name)[0]
}
