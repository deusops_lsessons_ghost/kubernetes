#============================= Yandex Cloud variables ==============================#

variable "zone" {
  description = "Deafult zone"
  default     = "ru-central1-a"
}

variable "yandex-token" {
  description = "Yandex token"
}

variable "yandex-cloud-id" {
  description = "Yandex cloud id"
}

variable "yandex-folder-id" {
  description = "Yandex folder id"
}

variable "yandex-service-account-id" {
  description = "Yandex service account id"
}

#=============================== Instances variables ==================================#

variable "instance-platform-id" {
  description = "Type of instance CPUs"
  type        = string
  default     = "standard-v3"
}

variable "instance-cores" {
  description = "Amount of CPU cores"
  type        = number
  default     = 2
}

variable "instance-memory" {
  description = "Amount of memory, GB"
  type        = number
  default     = 1
}

variable "instance-core-fraction" {
  description = "Fraction per core, %"
  type        = number
  default     = 20
}

variable "instance-disk-type" {
  description = "Type of disk (HDD, SSD)"
  type        = string
  default     = "network-hdd"
}

variable "is-auto-shutdown" {
  description = "Is 24h autoshutdown true"
  type        = bool
  default     = true
}

variable "instance-user-template" {
  description = "Creat new user on each instance"
  default     = "./template/ssh_user.tpl"
}

#------------------- PostgreSQL -------------------#

variable "yandex-db-name" {
  description = "Name of yandex resource"
  type        = string
  default     = "postgresql"
}

variable "db-environment" {
  description = "Environment of DataBase, may be 'PRODUCTION' or 'PRESTABLE'"
  type        = string
  default     = "PRODUCTION"
}

variable "db-version" {
  description = "Version of PostgreSQL DB"
  type        = number
  default     = 14
}

variable "db-platform-preset" {
  description = "Type of instance preset"
  type        = string
  default     = "s2.micro"
}

variable "db-disk-type" {
  description = "Type of disk (HDD, SSD)"
  type        = string
  default     = "network-ssd"
}

variable "db-disk-size" {
  description = "Size of disk, GB"
  type        = number
  default     = 15
}

variable "opencart-db" {
  description     = "Opencart DataBase variables"
  type = object({
    db_name          = string
    db_user_name     = string
    db_user_password = string
  })
  sensitive = true
}

variable "db-conn-limit" {
  description = "Limit connections to DB"
  type        = number
  default     = 15
}

variable "db-maintenance" {
  description = "DataBase maintenance window"
  type = map
  default = {
    type  = "WEEKLY"
    day   = "SAT"
    hour  = 12
  }
}

#----------------- k8s controller -----------------#

variable "controller-node-name" {
  description = "Name of control node"
  type        = string
  default     = "my-zonal-cluster"
}

variable "controller-node-description" {
  description = "Control node description"
  type        = string
  default     = "my-zonal-cluster description"
}

variable "k8s-version" {
  description = "Version of kubernetes cluster"
  type        = string
  default     = "1.22"
}

variable "controller-node-public-ip" {
  description = "Control node public IP"
  type        = bool
  default     = false
}

variable "k8s-release-channel" {
  description = "Kubernetes release channel, may be 'RAPID', 'REGULAR' or 'STABLE'"
  type        = string
  default     = "STABLE"
}

variable "k8s-agent-name" {
  description = "Name of kubernetes controller agent"
  type        = string
  default     = "k8s-agent"
}

variable "k8s-agent-description" {
  description = "Description for k8s agent"
  type        = string
  default     = "k8s service account"
}

#--------------------- Worker nodes ---------------------#

variable "worker-node-group-name" {
  description = "Name of node group"
  type        = string
  default     = "my-node-group"
}

variable "worker-node-group-description" {
  description = "Worker node group description"
  type        = string
  default     = "my-node-group-description"
}

variable "worker-node-public-ip" {
  description = "Control node public IP"
  type        = bool
  default     = false
}

variable "worker-node-ssh-user" {
  description = "Node instance user name"
  type        = string
  default     = "ghost_n"
}

variable "worker-node-ssh-key-path" {
  description = "Node instance ssh public key path"
  type        = string
  default     = "~/.ssh/node_rsa"
}

variable "worker-node-ssh-key-name" {
  description = "Node instance ssh public key path"
  type        = string
  default     = "node_rsa"
}

variable "worker-node-disk-size" {
  description = "Minimal disk size of the node, GB"
  type        = number
  default     = 30
}

variable "worker-nodes-scale-policy" {
  description = "Number of nodes"
  type        = map
  default = {
    min       = 1
    max       = 2
    initial   = 1
  }
}

variable "worker-node-maintenance-policy" {
  description     = "Maintenance policy for worker nodes"
  type = object({
    auto_upgrade  = bool
    auto_repair   = bool
  })
  default = {
    auto_repair   = true
    auto_upgrade  = true
  }
}

variable "worker-nodes-maintenance_window" {
  description  = "Maintenance window for worker nodes"
  type         = map
  default = {
    day        = "sunday"
    start_time = "03:00"
    duration   = "3h"
  }
}

variable "worker-nodes-labels" {
  description = "Labels for worker nodes"
  type        = map
  default = {
    key = "label"
  }
}

#--------------------- Entrypoint ---------------------#

variable "entrypoint-ssh-user" {
  description = "Entrypoint instance user name"
  type        = string
  default     = "ghost_e"
}

variable "entrypoint-ssh-key-path" {
  description = "Entrypoint instance ssh public key path"
  type        = string
  default     = "~/.ssh/entrypoint_rsa"
}

variable "entrypoint-instance-name" {
  description = "Name of entrypoint instance"
  type        = string
  default     = "entrypoint"
}

variable "entrypoint-instance-description" {
  description = "Entrypoint instance description"
  type        = string
  default     = "Entrypoint instance"
}

variable "entrypoint-image-id" {
  # Yandex Cloud Toolbox image based on Ubuntu 22.04 LTS
  # family_id: toolbox
  description = "Yandex Cloud Toolbox image"
  type        = string
  default     = "fd88piqslhj0th16uage"
}

variable "entrypoint-public-ip" {
  description = "Entrypoint public IP"
  type        = bool
  default     = true
}

variable "yc-toolbox-disk-size" {
  description = "Minimal size of the disk, GB"
  type        = number
  default     = 30
}
