output "external_entrypoint_ip" {
  value = yandex_compute_instance.entrypoint.network_interface.0.nat_ip_address
}

output "mdb-cluster_id" {
  value = yandex_mdb_postgresql_cluster.postgresql.id
}

output "mk8s-cluster_id" {
  value = yandex_kubernetes_cluster.k8s-zonal-cluster.id
}

output "opencart-db" {
  value = {
    dbname     = local.dbname
    dbuser     = local.dbuser
    dbpassword = (local.dbpassword)
    dbhosts    = join(",", local.dbhosts)
  }
  sensitive = true
}
