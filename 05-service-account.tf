resource "yandex_iam_service_account" "k8s-agent-account" {
  name        = var.k8s-agent-name
  description = var.k8s-agent-description
}

# resource "yandex_resourcemanager_folder_iam_binding" "k8s-admin" {
#   # Сервисному аккаунту назначается роль admin для тестов!!!.
#   folder_id = var.yandex-folder-id
#   role      = "admin"
#   members = [
#     "serviceAccount:${yandex_iam_service_account.k8s-agent-account.id}"
#   ]
# }

resource "yandex_resourcemanager_folder_iam_binding" "k8s-clusters-agent" {
  # Сервисному аккаунту назначается роль "k8s.clusters.agent".
  folder_id = var.yandex-folder-id
  role      = "k8s.clusters.agent"
  members = [
    "serviceAccount:${yandex_iam_service_account.k8s-agent-account.id}"
  ]
}

resource "yandex_resourcemanager_folder_iam_binding" "vpc-public-admin" {
  # Сервисному аккаунту назначается роль "vpc.publicAdmin".
  folder_id = var.yandex-folder-id
  role      = "vpc.publicAdmin"
  members = [
    "serviceAccount:${yandex_iam_service_account.k8s-agent-account.id}"
  ]
}

resource "yandex_resourcemanager_folder_iam_binding" "images-puller" {
  # Сервисному аккаунту назначается роль "container-registry.images.puller".
  folder_id = var.yandex-folder-id
  role      = "container-registry.images.puller"
  members = [
    "serviceAccount:${yandex_iam_service_account.k8s-agent-account.id}"
  ]
}
