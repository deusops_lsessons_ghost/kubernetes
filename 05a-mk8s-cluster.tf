data "template_file" "node_user" {
  template    = file(var.instance-user-template)
  vars = {
    ssh_user  = var.worker-node-ssh-user
    ssh_key   = file("${var.worker-node-ssh-key-path}.pub")
  }
}

resource "yandex_kubernetes_cluster" "k8s-zonal-cluster" {
  name          = var.controller-node-name
  description   = var.controller-node-description
  network_id    = yandex_vpc_network.network_terraform.id
  master {
    version     = var.k8s-version
    zonal {
      zone      = yandex_vpc_subnet.subnet_terraform.zone
      subnet_id = yandex_vpc_subnet.subnet_terraform.id
    }
    public_ip   = var.controller-node-public-ip
  }
  service_account_id      = yandex_iam_service_account.k8s-agent-account.id
  node_service_account_id = yandex_iam_service_account.k8s-agent-account.id
  release_channel         = var.k8s-release-channel

  depends_on = [
    yandex_resourcemanager_folder_iam_binding.k8s-clusters-agent,
    yandex_resourcemanager_folder_iam_binding.vpc-public-admin,
    yandex_resourcemanager_folder_iam_binding.images-puller
  ]

  kms_provider {
    key_id = yandex_kms_symmetric_key.kms-key.id
  }
}

resource "yandex_kms_symmetric_key" "kms-key" {
  # Ключ для шифрования важной информации, такой как пароли, OAuth-токены и SSH-ключи.
  name              = "kms-key"
  default_algorithm = "AES_128"
  rotation_period   = "8760h" # 1 год.
}

resource "yandex_kubernetes_node_group" "k8s-node-group" {
  cluster_id  = yandex_kubernetes_cluster.k8s-zonal-cluster.id
  name        = var.worker-node-group-name
  description = var.worker-node-group-description
  version     = var.k8s-version

  labels = var.worker-nodes-labels

  instance_template {
    platform_id = var.instance-platform-id

    network_interface {
      subnet_ids    = [yandex_vpc_subnet.subnet_terraform.id]
      nat           = var.worker-node-public-ip
    }

    resources {
      memory        = var.instance-memory
      cores         = var.instance-cores
      core_fraction = var.instance-core-fraction
    }

    boot_disk {
      type          = var.instance-disk-type
      size          = var.worker-node-disk-size
    }

    scheduling_policy {
      preemptible   = var.is-auto-shutdown
    }
    
    metadata = {
      user-data = data.template_file.node_user.rendered
    }
  }

  scale_policy {
    auto_scale {
      min     = var.worker-nodes-scale-policy.min
      max     = var.worker-nodes-scale-policy.max
      initial = var.worker-nodes-scale-policy.initial
    }
  }

  allocation_policy {
    location {
      zone = var.zone
    }
  }

  maintenance_policy {
    auto_upgrade = var.worker-node-maintenance-policy.auto_upgrade
    auto_repair  = var.worker-node-maintenance-policy.auto_repair

    maintenance_window {
      day        = var.worker-nodes-maintenance_window.day
      start_time = var.worker-nodes-maintenance_window.start_time
      duration   = var.worker-nodes-maintenance_window.duration
    }
  }
}
