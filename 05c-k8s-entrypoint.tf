data "template_file" "entrypoint_user" {
  template = file(var.instance-user-template)
  vars = {
    ssh_user  = var.entrypoint-ssh-user
    ssh_key   = file("${var.entrypoint-ssh-key-path}.pub")
  }
}

resource "yandex_compute_instance" "entrypoint" {
  name            = var.entrypoint-instance-name
  hostname        = var.entrypoint-instance-name
  description     = var.entrypoint-instance-description

  service_account_id        = yandex_iam_service_account.k8s-agent-account.id
  platform_id               = var.instance-platform-id
  allow_stopping_for_update = true

  depends_on = [
    yandex_mdb_postgresql_database.db-opencart,
    yandex_mdb_postgresql_user.user-opencart,
    yandex_kubernetes_node_group.k8s-node-group
  ]

  resources {
    cores         = var.instance-cores
    memory        = var.instance-memory
    core_fraction = var.instance-core-fraction
  }

  boot_disk {
    initialize_params {
      image_id    = var.entrypoint-image-id
      type        = var.instance-disk-type
      size        = var.yc-toolbox-disk-size
    }
  }

  network_interface {
    subnet_id     = yandex_vpc_subnet.subnet_terraform.id
    nat           = var.entrypoint-public-ip
  }

  scheduling_policy {
    preemptible    = var.is-auto-shutdown
  }

  metadata = {
    user-data      = data.template_file.entrypoint_user.rendered
  }
}

resource "null_resource" "entrypoint_provisioning" {
  depends_on = [
    yandex_compute_instance.entrypoint
  ]
  provisioner "file" {
    source        = var.worker-node-ssh-key-path
    destination   = "/home/${var.entrypoint-ssh-user}/.ssh/${var.worker-node-ssh-key-name}"
  
    connection {
      host        = yandex_compute_instance.entrypoint.network_interface.0.nat_ip_address
      type        = "ssh"
      user        = var.entrypoint-ssh-user
      private_key = file(var.entrypoint-ssh-key-path)
    }
  }

  provisioner "remote-exec" {
    on_failure    = continue # Скрипт выбрасывает из сессии тем самым вызывая ошибку работы терраформ
    inline =[
      "chmod 600 /home/${var.entrypoint-ssh-user}/.ssh/${var.worker-node-ssh-key-name}",
      "/usr/local/etc/setup.sh" 
    ]

    connection {
      host        = yandex_compute_instance.entrypoint.network_interface.0.nat_ip_address
      type        = "ssh"
      user        = var.entrypoint-ssh-user
      private_key = file(var.entrypoint-ssh-key-path)
    }
  }
  provisioner "remote-exec" {
    on_failure    = continue
    inline =[
      "yc managed-kubernetes cluster get-credentials --id=${yandex_kubernetes_cluster.k8s-zonal-cluster.id} --internal"
    ]

    connection {
      host        = yandex_compute_instance.entrypoint.network_interface.0.nat_ip_address
      type        = "ssh"
      user        = var.entrypoint-ssh-user
      private_key = file(var.entrypoint-ssh-key-path)
    }
  }
}
